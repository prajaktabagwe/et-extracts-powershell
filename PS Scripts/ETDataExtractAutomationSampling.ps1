﻿#ET Extract Date Range Control File
$TrackingDurationFile = 'C:\eSample Exact Target Email Activity Data Download\TrackingDatesSampling.csv'

#ET Configs
$ETConfigFile = 'C:\eSample Exact Target Email Activity Data Download\EmailTrackerApp.config'
$StartDateRegex = '(?<=<add key="TrackFromDate" value=")[^"]*'
$EndDateRegex = '(?<=<add key="TrackToDate" value=")[^"]*'
$ETDataExtractApp = "C:\eSample Exact Target Email Activity Data Download\PIH.PIPlatform.ExactTargetEmailTracker"
$ETDataExtractLocation = 'C:\ET-DataDownload\ET-Sampling\23529'

#S3 Configs
#$awsCredentialProfile = 'default'
$s3DropLocation = "s3://aptus-warehouse-drop/prod/exacttarget-sampling/drop/"
try {

   # Set-AWSCredential -ProfileName $awsCredentialProfile

    $TrackingDurationFileCSV = Import-Csv $TrackingDurationFile 
    foreach ($dateRange in $TrackingDurationFileCSV) {
        
        Write-Host (Get-Date).ToString()
        Write-Host "Starting Date Range From : $($dateRange.StartDate), To : $($dateRange.EndDate)"
        
        # Update ET Config with new Date Range
        Write-Host (Get-Date).ToString()
        Write-host $ETConfigFile 
        Write-Host "Updating ET config file with new Date Range"
        (Get-Content $ETConfigFile) -replace $StartDateRegex,$dateRange.StartDate | Set-Content $ETConfigFile
        (Get-Content $ETConfigFile) -replace $EndDateRegex,$dateRange.EndDate | Set-Content $ETConfigFile
         Write-Host "Updating ET config file with new Date Range done"
        # Run the extract
        $StartDate = (Get-Date).ToString()
        Write-Host $StartDate
        Write-Host "Start ET Data extract"
         Write-Host $ETDataExtractApp
        & $ETDataExtractApp
        Write-Host (Get-Date).ToString()
        Write-Host "ET Data extract Completed"

        # Upload to S3
        $StartDate = (Get-Date).ToString()
        Write-Host $StartDate
        Write-Host "Starting  Upload to S3"
        aws s3 mv $ETDataExtractLocation $s3DropLocation --recursive
        Write-Host (Get-Date).ToString()
        Write-Host "Upload to S3 Completed"

    } 
} catch {
    Write-Error Get-Date + " : " + $_.Exception.Message
}
