﻿#ET Extract Date Range Control File
$TrackingDurationFile = 'C:\myPI Exact Target Email Activity Data Download\TrackingDates.csv'

#ET Configs
$ETConfigFile = 'C:\myPI Exact Target Email Activity Data Download\EmailTrackerApp.config'
$StartDateRegex = '(?<=<add key="TrackFromDate" value=")[^"]*'
$EndDateRegex = '(?<=<add key="TrackToDate" value=")[^"]*'
$ETDataExtractApp = "C:\myPI Exact Target Email Activity Data Download\PIH.PIPlatform.ExactTargetEmailTracker"
$ETDataExtractLocation = 'C:\ET-DataDownload\ET-MyPI\20083'

#S3 Configs
$awsCredentialProfile = 'default'
$s3DropLocation = "s3://aptus-warehouse-drop/niraj-temp/PI/"
try {

    Set-AWSCredential -ProfileName $awsCredentialProfile

    $TrackingDurationFileCSV = Import-Csv $TrackingDurationFile 
    foreach ($dateRange in $TrackingDurationFileCSV) {
        
        Write-Host (Get-Date).ToString()
        Write-Host "Starting Date Range From : $($dateRange.StartDate), To : $($dateRange.EndDate)"
        
        # Update ET Config with new Date Range
        Write-Host (Get-Date).ToString()
        Write-Host "Updating ET config file with new Date Range"
        (Get-Content $ETConfigFile) -replace $StartDateRegex,$dateRange.StartDate | Set-Content $ETConfigFile
        (Get-Content $ETConfigFile) -replace $EndDateRegex,$dateRange.EndDate | Set-Content $ETConfigFile

        # Run the extract
        $StartDate = (Get-Date).ToString()
        Write-Host $StartDate
        Write-Host "Start ET Data extract"
        & $ETDataExtractApp
        Write-Host (Get-Date).ToString()
        Write-Host "ET Data extract Completed"

        # Upload to S3
        $StartDate = (Get-Date).ToString()
        Write-Host $StartDate
        Write-Host "Starting  Upload to S3"
        aws s3 mv $ETDataExtractLocation $s3DropLocation --recursive
        Write-Host (Get-Date).ToString()
        Write-Host "Upload to S3 Completed"

    } 
} catch {
    Write-Error Get-Date + " : " + $_
}
